'use strict';
var minimatch = require('minimatch'),
         path = require('path');

module.exports = plugin;

function plugin(options) {
  return function(files, metalsmith, done) {
    Object.keys(options).forEach(function(opt) {
      var matcher = minimatch.Minimatch(options[opt].filepath);

      Object.keys(files).forEach(function(file) {
        if (!matcher.match(file)) {
          return;
        }

        var filename = options[opt].filename;
        var metadata = metalsmith.metadata();
        var random = Math.floor((Math.random() * 100000) + 1);
        var k = filename.replace(/\./g, '').replace(/-/g, '').replace(/_/g, '');
        metadata[k] = random+'-'+filename;
        filename = random+'-'+filename;

        var renamedEntry = path.dirname(file) + '/';

        if (typeof filename === 'function') {
          renamedEntry += filename(path.basename(file));
        } else {
          renamedEntry += filename;
        }

        files[renamedEntry] = files[file];
        delete files[file];
      });
    });
    done();
  };
}
