

# Cache busting plugin for Metalsmith

Based on [metalsmith-renamer](https://github.com/alex-ketch/metalsmith-renamer)

## What it does

Adds a random string consisting of numbers to any file specified and removes the original file from the built. Provides a metalsmith metadata variable for usage in template files.

## Usage

F.e. global.css becomes 43757-global.css

You can then use file name stripped of all special characters in your template:

``` Handlebars

{{globalcss}}

```

This would refer to the cache-busted file 43757-global.css in this Handlebars example.  

Add the plugin to Metalsmith like any other metalsmith plugin, then define

- the filepath to the file you want to cache bustify,

- the filename of the file to be cache-busted


``` JSON

{
  "metalsmith-cache-buster": {
    "global-css-renaming": {
      "filepath": "assets/css/global.css",
      "filename": "global.css"
    },
    "rtl-css-renaming": {
      "filepath": "assets/css/rtl.css",
      "filename": "rtl.css"
    }
  }
}

```
